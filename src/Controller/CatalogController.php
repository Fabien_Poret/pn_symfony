<?php

namespace App\Controller;

use App\Repository\AdRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CatalogController extends AbstractController
{
    /**
     * @Route("/", name="catalog")
     */
    public function index(AdRepository $adRepository)
    {
        $kayaks = $adRepository->find4lastProduct('kayak');
        $canoes = $adRepository->find4lastProduct('canoë');
        $equipements = $adRepository->find4lastProduct('equipements');

        return $this->render('catalog/catalog.html.twig', [
            'controller_name' => 'CatalogController',
            'kayaks' => $kayaks, 
            'canoes' => $canoes,
            'equipements' => $equipements
        ]);
    }
    
    /**
     * @Route("/history", name="catalog_history")
     */
    public function history()
    {
        return $this->render('catalog/history.html.twig', [
            'controller_name' => 'CatalogController',
        ]);
    }
}
