<?php

namespace App\Controller;

use App\Entity\User;
use App\Entity\Message;
use App\Util\Fonctions;
use App\Form\MessageType;
use App\Repository\MessageRepository;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security as SymfonySecurity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/message")
 */
class MessageController extends AbstractController
{
    private $security;
    public function __construct(SymfonySecurity $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/{type}", name="message_index", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function index(MessageRepository $messageRepository, $type = ""): Response
    {
        $user = $this->security->getUser();
        
        dump($type);

        if ($type == "reçus") {
            $messages = array_reverse($messageRepository->findBy(['sendTo' => $user]));
        } else {
            $messages = array_reverse($messageRepository->findBy(['sendBy' => $user]));
        }

        return $this->render('message/index.html.twig', [
            'type' => $type,
            'messages' => $messages,
        ]);
    }

    /**
     * @Route("/new/{id}", name="message_new", methods={"GET","POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request, User $user, \Swift_Mailer $mailer, UserRepository $userRepository): Response
    {
        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            // $message->
            $mail_admin = Fonctions::getEnv('mail_admin');

            $admin = $userRepository->findOneBy(['email' => $mail_admin]);

            $message->setSendAt(new \DateTime())
            ->setSendBy($user)
            ->setSendTo($admin)
            ->setMessageRead(false);
            // ->setDog($user->getDog());
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();
            
            // TODO: Mail

            $href = $this->generateUrl('message_index', [
        
            ], UrlGeneratorInterface::ABSOLUTE_URL);
    
            $lien='<br><a href="'.$href.'">Afficher mes messages</a>';
            //Envoi du message

            // TODO: Ajouter un lien pour répondre
            $mail = new \Swift_Message("Nouveau message disponible sur les PucesNautiques");
            $mail->setFrom($mail_admin);
            $mail->setTo([$user->getEmail() => 'Utilisateur']);
            $mail->setBody("
                <h1>" . $message->getTitle() . "</h1>
        
                <br>
                Envoyé le : " . $message->getSendAt()->format('H:i:s d-m-Y ') . "
                <br>
                <br>
                " . $message->getContent() . "
                <br>
                <br>
                " . $message->getSendBy() . "
                ",
                'text/html'
            );
            try {
                $retour=$mailer->send($mail);
            }
            catch (\Swift_TransportException $e) {
                $retour= $e->getMessage();
            }
            
            $this->addFlash('success', 'Votre message à bien été envoyé');
            return $this->redirectToRoute('message_index');
        }

        return $this->render('message/new.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/show/{id}", name="message_show", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function show(Message $message): Response
    {
        $message->setMessageRead(true);
        
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($message);
        $entityManager->flush();

        return $this->render('message/show.html.twig', [
            'message' => $message,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="message_edit", methods={"GET","POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function edit(Request $request, Message $message): Response
    {
        $form = $this->createForm(MessageType::class, $message);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('message_index');
        }

        return $this->render('message/edit.html.twig', [
            'message' => $message,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/delete/{id}", name="message_delete")
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function delete(Request $request, Message $message): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($message);
        $entityManager->flush();

        $this->addFlash('warning', 'Votre message à bien été supprimé');
    
        return $this->redirectToRoute('message_index');
    }
}
