<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\UserType;
use App\Data\SearchData;
use App\Form\SearchType;
use App\Form\SearchByType;
use App\Service\FileUploader;
use App\Form\SearchByUserType;
use App\Form\SearchByTitleType;
use App\Repository\UserRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

/**
 * @Route("/user")
 */
class UserController extends AbstractController
{
    /**
     * @Route("/", name="user_index", methods={"GET"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function index(UserRepository $userRepository, Request $request): Response
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);
        $form = $this->createForm(SearchByUserType::class, $data);
        
        $form->handleRequest($request);
        $users = $userRepository->findSearch($data);

        return $this->render('user/overview.html.twig', [
            // 'users' => array_reverse($userRepository->findAll()),
            'users' => $users,
            'form' => $form->createView(),
        ]);
    }

    // TODO: Role admin ou utilisateur propriétaire 
    /**
     * @Route("/{id}", name="user_show", methods={"GET"})
     * @Security("user===userConnect or is_granted('ROLE_ADMIN')")
     */
    public function show(User $userConnect): Response
    {
        return $this->render('user/show.html.twig', [
            'user' => $userConnect,
        ]);
    }

    // TODO: Role admin ou utilisateur propriétaire 
    /**
     * @Route("/{id}/edit", name="user_edit", methods={"GET","POST"})
     * @Security("user===userConnect or is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, User $userConnect, UserPasswordEncoderInterface $passwordEncoder, FileUploader $fileUploader): Response
    {
        $form = $this->createForm(UserType::class, $userConnect);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $userConnect->setPassword(
                $passwordEncoder->encodePassword(
                    $userConnect,
                    $form->get('password')->getData()
                )
            );
            // TODO : mail
            $file = $form['image']->getData();           
            if($file){
                $res = $fileUploader->upload($file);
                if(is_string($res)){
                    // $article->setImage($res);
                    $userConnect->setImage($res);
                }else{
                    // $message = $res->getMessage();
                    $userConnect->setImage('');
                }
            }else{
                // $article->setImage(''); 
                $userConnect->setImage('');
            }
            $this->getDoctrine()->getManager()->flush();
            return $this->redirectToRoute('user_show', [
                'id' => $userConnect->getId()
            ]);
        }

        return $this->render('user/edit.html.twig', [
            'user' => $userConnect,
            'form' => $form->createView(),
        ]);
    }

    // TODO: Role admin ou utilisateur propriétaire 
    /**
     * @Route("/{id}/delete", name="user_delete")
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, User $user): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        foreach($user->getAds() as $ad){
            $entityManager->remove($ad);
        };
            // TODO : mail

        $entityManager->remove($user);
        $entityManager->flush();
    

        return $this->redirectToRoute('user_index');
    }

    // TODO: Role admin ou utilisateur propriétaire 
    /**
     * @Route("/ads/{id}", name="user_ad", methods={"GET"})
     * @Security("user===userConnect  or is_granted('ROLE_ADMIN')")
     */
    public function adByUser(User $userConnect, Request $request, UserRepository $userRepository): Response
    {
        // $data = new SearchData();
        // $data->page = $request->get('page', 1);
        // $form = $this->createForm(SearchByTitleType::class, $data);
        
        // $form->handleRequest($request);
        // $users = $userRepository->findSearch($data);
        
        return $this->render('user/own_ad.html.twig', [
            'user' => $userConnect,
            // 'form' => $form->createView(),
        ]);
    }
}
