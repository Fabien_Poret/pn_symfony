<?php

namespace App\Controller;

use App\Entity\Ad;
use Dompdf\Dompdf;
use App\Entity\User;
use App\Entity\Invoice;
use App\Form\InvoiceType;
use App\Repository\InvoiceRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Security\Core\Security as SymfonySecurity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/invoice")
 */
class InvoiceController extends AbstractController
{
    private $security;
    public function __construct(SymfonySecurity $security)
    {
        $this->security = $security;
    }
    
    /**
     * @Route("/", name="invoice_index", methods={"GET"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function index(InvoiceRepository $invoiceRepository): Response
    {
        // TODO : Pagination et recherche 
        // $data = new SearchData();
        // $data->page = $request->get('page', 1);
        // $form = $this->createForm(SearchType::class, $data);
        // $form->handleRequest($request);
        // [$min, $max] = $adRepository->findMinMax($data);
        // dump($min, $max);
        // $ads = $adRepository->findSearch($data);

        $user = $this->security->getUser();
        foreach ($user->getRoles() as $role) {
            if ($role != "ROLE_ADMIN") {
                $invoice = $user->getInvoices();
            }
            else{
                $invoice = $invoiceRepository->findAll();
            }
        }
        dump($invoice);

        return $this->render('invoice/index.html.twig', [
            'invoices' => $invoice,
        ]);
    }

    // TODO: Gérer la sécurité de cette route 
    /**
     * @Route("/new/{buyer}/{ad}", name="invoice_new", methods={"GET","POST"})
     */
    public function new(Request $request, User $buyer, Ad $ad): Response
    {
        // TODO: Faire la création d'une facture à l'achat
        // TODO: Désactiver la fiche produit quand l'achat est effectué
        $invoice = new Invoice();

        $entityManager = $this->getDoctrine()->getManager();

        $invoice->setBuyer($buyer)
            ->setProduct($ad)
            ->setSeller($ad->getUser());

        $entityManager->persist($invoice);
        $entityManager->flush();

        return $this->redirectToRoute('invoice_show', [
            'id' => $invoice->getId(),
        ]);
        
    }

    /**
     * @Route("/{id}", name="invoice_show", methods={"GET"})
     * @Security("user===invoice.getSeller() or user===invoice.getBuyer() or is_granted('ROLE_ADMIN')")
     */
    public function show(Invoice $invoice): Response
    {
        return $this->render('invoice/show.html.twig', [
            'invoice' => $invoice,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="invoice_edit", methods={"GET","POST"})
     *  @Security("is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, Invoice $invoice): Response
    {
        $form = $this->createForm(InvoiceType::class, $invoice);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('invoice_index');
        }

        return $this->render('invoice/edit.html.twig', [
            'invoice' => $invoice,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="invoice_delete", methods={"DELETE"})
     * @Security("is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, Invoice $invoice): Response
    {
        if ($this->isCsrfTokenValid('delete'.$invoice->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($invoice);
            $entityManager->flush();
        }

        return $this->redirectToRoute('invoice_index');
    }

    /**
     * @Route("/generate/{id}", name="invoice_generate", methods={"GET"})
     * @Security("user===invoice.getSeller() or user===invoice.getBuyer() or is_granted('ROLE_ADMIN')")
     */
    public function generateInvoice(Invoice $invoice): Response
    {
        // TODO: Mettre en page la facture

        $dompdf = new Dompdf();
        if ($invoice->getProduct()->getImage()) {
            $dompdf->loadHtml('
            <h1>Facture pour l\'annonce ' . $invoice->getProduct() .'</h1>
            <p>Date de la vente : 27/04/2020</p>
            <br>
            Vendeur : '. $invoice->getSeller() .' | Acheteur : ' . $invoice->getBuyer() . ' 
            <p>Prix : '. $invoice->getProduct()->getPrice() .'€</p>
            <br>
            <img style="width: 10%;" src="images/'.$invoice->getProduct()->getImage().'">
            '. $invoice->getProduct()->getDescription() .'
            <br>
            <h3>Modalités légales</h3>
            <p>Iure integer nascetur rutrum quis assumenda ultricies cumque condimentum cum do vitae ipsum volutpat. 
            Porro do class vulputate. Hac voluptates, error natoque accusamus per? Magnis aute condimentum deleniti l
            aborum mollit ut ultricies ac repellendus, facilisis. Natus! Dolorem quos mi? Minima rem, velit! Quidem vene
            natis ad maiores proin incidunt rhoncus nullam tortor veritatis penatibus quo, mattis ipsa voluptate autem do
            lores, facilisi metus cum blanditiis aptent. Hac, minima. Torquent autem laborum, eu, nonummy, semper hac rei
            ciendis sociosqu? Sem? Habitasse voluptatem, parturient totam ultricies neque quasi sapiente! Assumenda, adipis
            icing. In laoreet! Aliquam tenetur. Sit eu dolores accumsan wisi elit, maecenas, eligendi etiam, accusantium.</p>
            <br>
            <p>Les PucesNautiques - 2020</p>
            ');
        } else{
            $dompdf->loadHtml('
            <h1>Facture pour l\'annonce ' . $invoice->getProduct() .'</h1>
            <p>Date de la vente : 27/04/2020</p>
            <br>
            Vendeur : '. $invoice->getSeller() .' | Acheteur : ' . $invoice->getBuyer() . ' 
            <p>Prix : '. $invoice->getProduct()->getPrice() .'€</p>
            <br>
            '. $invoice->getProduct()->getDescription() .'
            <br>
            <h3>Modalités légales</h3>
            <p>Iure integer nascetur rutrum quis assumenda ultricies cumque condimentum cum do vitae ipsum volutpat. 
            Porro do class vulputate. Hac voluptates, error natoque accusamus per? Magnis aute condimentum deleniti l
            aborum mollit ut ultricies ac repellendus, facilisis. Natus! Dolorem quos mi? Minima rem, velit! Quidem vene
            natis ad maiores proin incidunt rhoncus nullam tortor veritatis penatibus quo, mattis ipsa voluptate autem do
            lores, facilisi metus cum blanditiis aptent. Hac, minima. Torquent autem laborum, eu, nonummy, semper hac rei
            ciendis sociosqu? Sem? Habitasse voluptatem, parturient totam ultricies neque quasi sapiente! Assumenda, adipis
            icing. In laoreet! Aliquam tenetur. Sit eu dolores accumsan wisi elit, maecenas, eligendi etiam, accusantium.</p>
            <br>
            <p>Les PucesNautiques - 2020</p>
            ');
        }
        

        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4');

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream();

        return $this->render('invoice/pdf.html.twig', [
        ]);
        
        // return $this->redirectToRoute('ad_show', [
        //     'id' => $invoice->getProduct()->getId(),
        // ]);

        // return $this->redirectToRoute('ad_index');
    }

}
