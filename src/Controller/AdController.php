<?php

namespace App\Controller;

use Exception;
use App\Entity\Ad;
use App\Entity\User;
use App\Form\AdType;
use App\Entity\Message;
use App\Data\SearchData;
use App\Form\MessageType;
use App\Form\SearchType;
use App\Service\FileUploader;
use App\Repository\AdRepository;
use Geocoder\Query\GeocodeQuery;
use Geocoder\Query\ReverseQuery;
use Http\Adapter\Guzzle6\Client;
use PhpParser\Node\Stmt\TryCatch;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Component\Security\Core\Security as SymfonySecurity;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
/**
 * @Route("/ad")
 */
class AdController extends AbstractController
{
    private $security;
    public function __construct(SymfonySecurity $security)
    {
        $this->security = $security;
    }

    /**
     * @Route("/", name="ad_index", methods={"GET"})
     */
    public function index(AdRepository $adRepository, Request $request): Response
    {
        $data = new SearchData();
        $data->page = $request->get('page', 1);
        $form = $this->createForm(SearchType::class, $data);
        $form->handleRequest($request);
        [$min, $max] = $adRepository->findMinMax($data);
        dump($min, $max);
        $ads = $adRepository->findSearch($data);

        return $this->render('ad/index.html.twig', [
            'ads' => $ads,
            // 'ads' => array_reverse($ads),
            // 'ads' => array_reverse($adRepository->findAll()),
            'form' => $form->createView(),
            'min' => $min,
            'max' => $max,
        ]);
    }

    /**
     * @Route("/overview", name="ad_overview", methods={"GET"})
     */
    public function overview(AdRepository $adRepository): Response
    {
        return $this->render('ad/overview.html.twig', [
            'ads' => array_reverse($adRepository->findAll()),
        ]);
    }


    /**
     * @Route("/new", name="ad_new", methods={"GET","POST"})
     * @Security("is_granted('IS_AUTHENTICATED_FULLY')")
     */
    public function new(Request $request, FileUploader $fileUploader): Response
    {
        $ad = new Ad();
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $ad->setUser($this->security->getUser())
                ->setCreatedAt(new \DateTime());

            $file = $form['fichierimage']->getData();           
            if($file){
                $res = $fileUploader->upload($file);
                if(is_string($res)){
                    // $article->setImage($res);
                    $ad->setImage($res);
                }else{
                    // $message = $res->getMessage();
                    $ad->setImage('');
                }
            }else{
                // $article->setImage(''); 
                $ad->setImage('');
            }

            $secondaryFile = $form['secondaryImage']->getData();           
            if($secondaryFile){
                $res = $fileUploader->upload($secondaryFile);
                if(is_string($res)){
                    // $article->setImage($res);
                    $ad->setSecondaryImage($res);
                }else{
                    // $message = $res->getMessage();
                    $ad->setSecondaryImage('');
                }
            }else{
                // $article->setImage(''); 
                $ad->setSecondaryImage('');
            }

            $thirdFile = $form['thirdImage']->getData();           
            if($thirdFile){
                $res = $fileUploader->upload($thirdFile);
                if(is_string($res)){
                    // $article->setImage($res);
                    $ad->setThirdImage($res);
                }else{
                    // $message = $res->getMessage();
                    $ad->setThirdImage('');
                }
            }else{
                // $article->setImage(''); 
                $ad->setThirdImage('');
            }

            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($ad);
            $entityManager->flush();

            // TODO: Mail
            $this->addFlash('success', 'Votre annonce à bien été créée');
            return $this->redirectToRoute('ad_show', [
                'id' => $ad->getId()
            ]);
        }

        return $this->render('ad/new.html.twig', [
            'ad' => $ad,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="ad_show")
     */
    public function show(Ad $ad, Request $request, \Swift_Mailer $mailer): Response
    {
        $httpClient = new \Http\Adapter\Guzzle6\Client();
        $provider = new \Geocoder\Provider\GoogleMaps\GoogleMaps($httpClient, null, 'AIzaSyCr5kIfYpbi33ninkBbTBZ-W7Y3Icc8Wko');
        $geocoder = new \Geocoder\StatefulGeocoder($provider, 'fr');
        try {
            // TODO: Bug sur les villes qui n'existe pas
            $result = $geocoder->geocodeQuery(GeocodeQuery::create($ad->getUser()->getCity()));
            // $result = $geocoder->geocodeQuery(GeocodeQuery::create('Dives-sur-mer'));
        } catch (Exception $e) {
            $result = $geocoder->geocodeQuery(GeocodeQuery::create('Lisieux'));
            dump($e);
        }

        $message = new Message();
        $form = $this->createForm(MessageType::class, $message);
        
        $form->handleRequest($request);
    
    
        if ($form->isSubmitted() && $form->isValid()) {
            $user = $this->security->getUser();
            $message->setSendAt(new \DateTime())
            ->setSendBy($user)
            ->setSendTo($ad->getUser())
            ->setAd($ad)
            ->setMessageRead(false);
            // ->setDog($user->getDog());
            
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($message);
            $entityManager->flush();
            
            // TODO: Mail

            $href = $this->generateUrl('message_index', [
            ], UrlGeneratorInterface::ABSOLUTE_URL);
    
            $lien='<br><a href="'.$href.'">Répondre à ce message</a>';
            //Envoi du message

            // TODO: Ajouter un lien pour répondre
            $mail = new \Swift_Message("Nouveau message disponible sur les PucesNautiques");
            $mail->setFrom($user->getEmail());
            $mail->setTo([$ad->getUser()->getEmail() => 'Utilisateur']);
            $mail->setBody("
                <h1>" . $message->getTitle() . "</h1>
                <br>
                Pour l'annonce : " . $message->getAd()->getTitle() ."
                <br>
                Envoyé le : " . $message->getSendAt()->format('H:i:s d-m-Y ') . "
                <br>
                <br>
                " . $message->getContent() . "
                <br>
                <br>
                ". $lien . "
                <br>
                <br>
                " . $message->getSendBy() . "
                ",
                'text/html'
            );
            try {
                $retour=$mailer->send($mail);
            }
            catch (\Swift_TransportException $e) {
                $retour= $e->getMessage();
            }
            
            $this->addFlash('success', 'Votre message à bien été envoyé');
            return $this->redirectToRoute("message_index", [
                'type' => 'envoyés'
            ]);
        }

        return $this->render('ad/show.html.twig', [
            'ad' => $ad,
            'latitude' => $result->first()->getCoordinates()->getLatitude(),
            'longitude' => $result->first()->getCoordinates()->getLongitude(),
            'form' => $form->createView(),
        ]);
    }

    // TODO: Role admin ou utilisateur propriétaire 
    /**
     * @Route("/{id}/edit/{userConnect}", name="ad_edit", methods={"GET","POST"})
     * @Security("user===userConnect or is_granted('ROLE_ADMIN')")
     */
    public function edit(Request $request, Ad $ad,  User $userConnect): Response
    {
        $form = $this->createForm(AdType::class, $ad);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            $this->addFlash('success', 'Votre annonce à bien été modifié');
            return $this->redirectToRoute('ad_show', [
                'id' => $ad->getId()
            ]);
        }

        // TODO: Mail
        
        return $this->render('ad/edit.html.twig', [
            'ad' => $ad,
            'form' => $form->createView(),
        ]);
    }

    // TODO: Role admin ou utilisateur propriétaire
    /**
     * @Route("/delete/{id}/{userConnect}", name="ad_delete")
     * @Security("user===userConnect or is_granted('ROLE_ADMIN')")
     */
    public function delete(Request $request, Ad $ad, User $userConnect): Response
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->remove($ad);
        $entityManager->flush();

        return $this->redirectToRoute('ad_index');
    }
}
