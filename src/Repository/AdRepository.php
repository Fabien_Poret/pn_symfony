<?php

namespace App\Repository;

use App\Entity\Ad;
use App\Data\SearchData;
use Doctrine\ORM\QueryBuilder;
use Knp\Component\Pager\PaginatorInterface;
use Doctrine\Common\Persistence\ManagerRegistry;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

/**
 * @method Ad|null find($id, $lockMode = null, $lockVersion = null)
 * @method Ad|null findOneBy(array $criteria, array $orderBy = null)
 * @method Ad[]    findAll()
 * @method Ad[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AdRepository extends ServiceEntityRepository
{
    /**
     * @var PaginatorInterface
     */
    private $paginator;

    public function __construct(ManagerRegistry $registry, PaginatorInterface $paginator)
    {
        parent::__construct($registry, Ad::class);
        $this->paginator = $paginator;
    }

    // /**
    //  * @return Ad[] Returns an array of Ad objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Ad
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    /**
     * Récupère les produits en lien avec une recherche
     * @return PaginationInterface
     */
    public function findSearch(SearchData $search): PaginationInterface
    {
    
        $query = $this->getSearchQuery($search)->getQuery();
        return $this->paginator->paginate(
            $query,
            $search->page,
            15
        );
    }

    /**
     * @param SearchData $search
     * @return integer[]
     */
    public function findMinMax(SearchData $search): array
    {
        $results = $this->getSearchQuery($search, true)
            ->select('MIN(ad.price) as min', 'MAX(ad.price) as max')
            ->getQuery()
            ->getScalarResult();
            
        return [(int)$results[0]['min'], $results[0]['max']];
    }
    
    /**
     * @param SearchData $search
     * @return integer[]
     */
    private function getSearchQuery(SearchData $search, $ignorePrice = false): QueryBuilder
    {
        $query = $this
        ->createQueryBuilder('ad')
        ->select('c', 'ad')
        ->join('ad.category', 'c');

        if(!empty($search->q))
        {
            $query = $query 
                ->andWhere('ad.title LIKE :q')
                ->setParameter('q', "%{$search->q}%");
        }

        if(!empty($search->brand))
        {
            $query = $query 
                ->andWhere('ad.brand LIKE :brand')
                ->setParameter('brand', "%{$search->brand}%");
        }

        if(!empty($search->min) && $ignorePrice === false )
        {
            $query = $query 
                ->andWhere('ad.price >= :min')
                ->setParameter('min', $search->min);
        }

        if(!empty($search->max) && $ignorePrice === false)
        {
            $query = $query 
                ->andWhere('ad.price <= :max')
                ->setParameter('max', $search->max);
        }

        if (!empty($search->categories)) {
            $query = $query
                ->andWhere('c.id IN (:categories)')
                ->setParameter('categories', $search->categories);
        }
        return $query;
    }

        //DQL SANS PARAMETRES
     /**
     * @return Article[] Returns an array of Article objects
     */
    public function find4lastProduct($category): array
    {
        $entityManager = $this->getEntityManager();

        $query = $entityManager->createQuery(
            "SELECT ad   
            FROM App\Entity\Ad ad
            INNER JOIN ad.category category
            WHERE category.title = " . "'". $category."'". "
            ORDER BY ad.id 
            DESC
            "
        )->setMaxResults(4);
        return $query->getResult();
    }


}
