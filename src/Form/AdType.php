<?php

namespace App\Form;

use App\Entity\Ad;
use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use FOS\CKEditorBundle\Form\Type\CKEditorType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class AdType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => '*Le nom de l\'objet',
            ])
            ->add('brand', TextType::class, [
                'label' => '*La marque de l\'objet',
            ])
            ->add('category',EntityType::class,[
                'class'=>Category::class,
                'label'=> '*Choix de la catégorie',
                'choice_label' => 'title',
                'placeholder' => 'Choix de la catégorie',
            ])
            ->add('description', CKEditorType::class, [
                'label' => '*La description de l\'objet',
            ])
            ->add('price', IntegerType::class, [
                'label' => '*Le prix de l\'objet en €',
            ])
            ->add('fichierimage', FileType::class, [
                'mapped' => false,
                'label' => 'Télécharger une image pour votre annonce en JPG',
                'required' => false,
                // Les champs non mappés ne peuvent pas utiliser les annotations pour les validations
                // dans les entités associées, nous devons donc utiliser des contraintes de classe
                // en utilisant le composant  Symfony\Component\Validator\Constraints\File;
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])
            ->add('secondaryImage', FileType::class, [
                'mapped' => false,
                'label' => 'Télécharger une deuxième image pour votre annonce en JPG',
                'required' => false,
                // Les champs non mappés ne peuvent pas utiliser les annotations pour les validations
                // dans les entités associées, nous devons donc utiliser des contraintes de classe
                // en utilisant le composant  Symfony\Component\Validator\Constraints\File;
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])
            ->add('thirdImage', FileType::class, [
                'mapped' => false,
                'label' => 'Télécharger une troisième image pour votre annonce en JPG',
                'required' => false,
                // Les champs non mappés ne peuvent pas utiliser les annotations pour les validations
                // dans les entités associées, nous devons donc utiliser des contraintes de classe
                // en utilisant le composant  Symfony\Component\Validator\Constraints\File;
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Ad::class,
        ]);
    }
}
