<?php

namespace App\Form;

use App\Entity\Category;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class CategoryType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'label' => '*Le nom de la catégorie',
            ])
            ->add('path', FileType::class, [
                'mapped' => false,
                'label' => 'Télécharger une image pour votre annonce',
                'required' => false,
                // Les champs non mappés ne peuvent pas utiliser les annotations pour les validations
                // dans les entités associées, nous devons donc utiliser des contraintes de classe
                // en utilisant le composant  Symfony\Component\Validator\Constraints\File;
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Category::class,
        ]);
    }
}
