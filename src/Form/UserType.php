<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Validator\Constraints\File;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;

class UserType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder

            ->add('firstName', TextType::class, [
                'label' => '*Votre prénom'
            ])
            ->add('lastName', TextType::class, [
                'label' => '*Votre nom'
            ])
            ->add('email', EmailType::class, [
                'label' => '*Votre adresse email'
            ])
            ->add('phone', IntegerType::class, [
                'label' => '*Votre numéro de téléphone'
            ])
            ->add('password', RepeatedType::class, [
                'type' => PasswordType::class,
                'invalid_message' => '*Les mots de passe ne sont pas identiques.',
                'options' => ['attr' => ['class' => 'password-field']],
                'required' => true,
                'first_options'  => ['label' => 'Votre mot de passe'],
                'second_options' => ['label' => 'Confirmez votre mot de passe'],
            ])
            ->add('streetAddress', TextType::class, [
                'label' => '*Votre adresse postal'
            ])
            ->add('postCode', IntegerType::class, [
                'label' => '*Votre code postal'
            ])
            ->add('city', TextType::class, [
                'label' => '*Votre ville'
            ])
            ->add('image', FileType::class, [
                'mapped' => false,
                'label' => 'Télécharger une image pour votre profil',
                'required' => false,
                // Les champs non mappés ne peuvent pas utiliser les annotations pour les validations
                // dans les entités associées, nous devons donc utiliser des contraintes de classe
                // en utilisant le composant  Symfony\Component\Validator\Constraints\File;
                'constraints' => [
                    new File([
                        'maxSize' => '5M',
                        'mimeTypes' => [
                            'image/jpeg'
                        ],
                        'mimeTypesMessage' => 'SVP Uploadez un fichier JPEG valide',
                    ])
                ],
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
