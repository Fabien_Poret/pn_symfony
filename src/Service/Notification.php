<?php 

namespace App\Service;

use DateTime;
use DateInterval;
use App\Entity\Message;
use App\Repository\UserRepository;
use App\Repository\ContractRepository;
use App\Repository\MessageRepository;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Security as SymfonySecurity;

class Notification extends AbstractController
{
    private $userRepository;
    // private $contractRepository;
    private $messageRepository;
    private $security;
    
    public function __construct(UserRepository $userRepository,  MessageRepository $messageRepository, SymfonySecurity $security) {
        $this->userRepository = $userRepository;
        $this->messageRepository = $messageRepository;
        $this->security = $security;
    }

    // public function getUsersCountNotValide()
    // {
    //     return count($this->userRepository->findBy(['validation' => '0']));
    // }

    // public function getUsersByEndContractSoon()
    // {
    //     //TODO : Code fonctionnel cependant pas optimisé. Trop long à charger
    //     // $contracts = $this->contractRepository->findAll();
    //     // $endContracts = [];
    //     // $dateTime = new DateTime();
    //     // foreach ($contracts as $contract) {
    //     //     if ($dateTime < $contract->getEndAt()) {
    //     //         if($dateTime > $contract->getEndAt()->sub(new DateInterval('P1M')))
    //     //         {
    //     //             dump($contract->getEndAt());
    //     //             array_push($endContracts, $contract);
    //     //         }
    //     //     }
    //     // }
    //     // return count($endContracts);
    //     return 2;
    // }
    public function getMessageCount()
    {
        //TODO : Code à faire. Trop long à charger
        $user = $this->security->getUser();
        return count($this->messageRepository->findBy(['messageRead' => '0', 'sendTo' => $user]));
        // return count($this->messageRepository->findBy(['messageRead' => '0']));
        // return 2;
    }



}