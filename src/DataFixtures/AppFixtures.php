<?php

namespace App\DataFixtures;

use App\Entity\Ad;
use App\Entity\Category;
use App\Entity\Invoice;
use App\Entity\Message;
use Faker\Factory;
use App\Entity\User;
use DateTime;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AppFixtures extends Fixture
{
    private $encoder;

    public function __construct(UserPasswordEncoderInterface $encoder)
    {
        $this->encoder = $encoder;
    }
    

    public function load(ObjectManager $manager)
    {
        // $product = new Product();
        // $manager->persist($product);
        $faker = \Faker\Factory::create("fr_FR");

        $admin = new User();
        $admin->setEmail("fabien.poret@outlook.fr")
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setLastName('Fabien')
            ->setFirstName('PORET')
            ->setPhone('0695060476')
            ->setStreetAddress($faker->streetAddress())
            ->setPostCode(14100)
            ->setCity('Lisieux')
            ->setPassword($this->encoder->encodePassword($admin,'admin76'));
        $manager->persist($admin);

        $admin = new User();
        $admin->setEmail("info@canoe-shop.om")
            ->setRoles(['ROLE_USER', 'ROLE_ADMIN'])
            ->setLastName('Menbeuf')
            ->setFirstName('Jean-Michel')
            ->setPhone('0695060476')
            ->setStreetAddress($faker->streetAddress())
            ->setPostCode(14100)
            ->setCity('Lisieux')
            ->setPassword($this->encoder->encodePassword($admin,'menbeuf1414'));
        $manager->persist($admin);


        $category1 = new Category();
        $category1->setTitle('Kayak');
        $manager->persist($category1);

        $category2 = new Category();
        $category2->setTitle('Canoë');
        $manager->persist($category2);

        $category3 = new Category();
        $category3->setTitle('Equipements');
        $manager->persist($category3);

        for ($u=0; $u < 50; $u++) { 
            $user = new User();
            $user->setEmail($faker->email())
                ->setRoles(['ROLE_USER'])
                ->setLastName($faker->lastName())
                ->setFirstName($faker->firstNameMale())
                ->setPhone($faker->phoneNumber())
                ->setStreetAddress($faker->streetAddress())
                ->setPostCode(14100)
                ->setImage('5e96e6c9b6d93.jpeg')
                ->setCity('Rouen')
                ->setPassword($this->encoder->encodePassword($user,'admin76'));
            $manager->persist($user);

            $message = new Message();
            $message->setSendBy($user)
                ->setSendTo($user)
                ->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true))
                ->setContent($faker->paragraph($nbSentences = 3, $variableNbSentences = true))
                ->setSendAt(new DateTime())
                ->setMessageRead(false);
            $manager->persist($message);

            for ($a=0; $a < 5; $a++) { 
                $ad = new Ad();
                $ad->setTitle($faker->sentence($nbWords = 6, $variableNbWords = true))
                    ->setDescription("Exercitation, deleniti cras exercitation deleniti quia sodales quis pretium esse, sit nostrum ridiculus viverra, eum pulvinar. Et repellendus! Nobis, ornare. Cumque proident! Proin molestie quis laoreet. Nostrud consequatur? Possimus netus? Accumsan at quis nesciunt mollit veritatis mollit nam parturient arcu doloribus cum, etiam possimus massa. Condimentum nostra at. Ipsum, sunt litora hymenaeos magnis excepteur. Potenti? Purus. Fusce officia aliqua voluptatibus repellendus rutrum, tincidunt! Hymenaeos! Velit ex tempus sapiente arcu molestiae, habitant vitae! Dignissim neque beatae? Sequi itaque non veritatis numquam nam eius dui posuere? Nostrum nisi senectus. Fuga. Potenti quaerat pellentesque sollicitudin, error ea. Lacus dicta, dolores sed fusce! Rutrum.")
                    ->setPrice($faker->randomNumber(3))
                    ->setBrand($faker->word())
                    ->setCreatedAt($faker->dateTimeBetween('-3 months'))
                    ->setImage('product_10544354b.jpg')
                    ->setSecondaryImage('519633.jpg')
                    ->setThirdImage('descente-en-canoe-kayak-depuis-les-ponts-de-ce-30km-angers-1549111358.jpeg')
                    ->setUser($user);

                if ($a < 2) {
                    $ad->setCategory($category1);
                }
                elseif ($a < 4) {
                    $ad->setCategory($category2);
                }
                else {
                    $ad->setCategory($category3);
                }
                $manager->persist($ad);

                $invoice = new Invoice();
                $invoice->setBuyer($user)
                    ->setSeller($admin)
                    ->setProduct($ad);
                $manager->persist($invoice);
            }
        }
        $manager->flush();
    }
}
